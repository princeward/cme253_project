 /*
 *  Copyright 2016 NVIDIA Corporation
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include <iostream>
#include <fstream>
#include <stdio.h>
#include "cublas_v2.h"
#include "debug.h"
#include "../Images/image_02/im_g_1024_1024.h"
#include <math.h>

using namespace std;

#define SIZE 1024 // assume square image
#define THREADS_PER_BLOCK_X 32
#define THREADS_PER_BLOCK_Y 32
#define S  256 //128 // we check 2S neighborhood 
#define W1 1.0 // weight on x, y distance
#define W2 1.0 // weight on color
#define SIZE_CLS 8 // size of the cluster, # of cluster = SIZE_CLS^2

typedef struct {
  double x;
  double y;
  double clr;
  double sum_x; // used for updating center
  double sum_y;
  double sum_clr;
  double cnt;
} ClusterCenter;

// __device__ ClusterCenter *global_center; // question: do we need device variable?

/*
__device__ double global_center[64][3+1]; // on global memory, visible to all threads
__device__ double global_sum[64][3+1]; // used for updating center
__device__ double global_cluster_cnt[64];
__device__ const int *d_img;
*/

__global__ void init_centers(ClusterCenter *global_center, int *d_img) {
  // Note: this kernel uses 1 block and 2D threads (8x8)!
  int x = threadIdx.x;
  int y = threadIdx.y;
  int idx = x * blockDim.x + y;
  global_center[idx].x = 64 + SIZE/8 * x;
  global_center[idx].y = 64 + SIZE/8 * y;
  global_center[idx].clr = d_img[ (int)global_center[idx].x * SIZE + (int)global_center[idx].y ];
  global_center[idx].sum_x = 0.0;
  global_center[idx].sum_y = 0.0;
  global_center[idx].sum_clr = 0.0;
  global_center[idx].cnt = 0.0;
}

__global__ void cluster_assign(ClusterCenter *global_center, 
                               int *d_img, 
                               int *result) {  
  __shared__ double center[64][3+1];
  __shared__ double local_sum[64][3+1]; // used for updating center
  __shared__ double local_cluster_cnt[64];   

  // read global centers to local (to gain speed)
  if(threadIdx.x < 2) { // only need 64 threads, question: is this the correct way?
    int cls_idx = threadIdx.x * 32 + threadIdx.y;
    center[cls_idx][0] = global_center[cls_idx].x;
    center[cls_idx][1] = global_center[cls_idx].y;
    center[cls_idx][2] = global_center[cls_idx].clr;

    // BTW, initialize other shared variables, since they can't be initialized upon declaration
    local_sum[cls_idx][0] = 0.0; // x
    local_sum[cls_idx][1] = 0.0; // y
    local_sum[cls_idx][2] = 0.0; // color
    local_cluster_cnt[cls_idx] = 0.0;
  }
  __syncthreads();

  const int x = blockIdx.x * THREADS_PER_BLOCK_X + threadIdx.x; // question???
  const int y = blockIdx.y * THREADS_PER_BLOCK_Y + threadIdx.y;  
  int assign = -1; // cluster assignment

  // read pixel value for the thread
  int px = d_img[x * SIZE + y];

  // calculate distance and assign clusters
  double dist = 0.0;
  double min_dist = 100000000.0;
  for(int i=0; i<64; i++) {
    if( (x>center[i][0]-S) && (x<center[i][0]+S) && (y>center[i][1]-S) && (y<center[i][1]+S) ) { // only consider 2S neighborhood
      double temp = (x-center[i][0])*(x-center[i][0]) + (y-center[i][1])*(y-center[i][1]);
      dist = W1*sqrt( temp ) + W2*abs( px-center[i][2] );
      if(dist < min_dist) {
        min_dist = dist;
        assign = i;
      }
    }
  }  

  // save to result
  result[x * 1024 + y] = assign;

  // prepare to update centers
  // update local sum (within the block)
  local_sum[assign][0] += x;
  local_sum[assign][1] += y;
  local_sum[assign][2] += px;
  local_cluster_cnt[assign] += 1.0;
  __syncthreads(); // wait until the full local_sum is obtained

  // 64 threads reports local sum to global center on behalf of the block
  if(threadIdx.x < 2) {
    int cls_idx = threadIdx.x * 32 + threadIdx.y;
    global_center[cls_idx].sum_x += local_sum[cls_idx][0];
    global_center[cls_idx].sum_y += local_sum[cls_idx][1];
    global_center[cls_idx].sum_clr += local_sum[cls_idx][2];
    global_center[cls_idx].cnt += local_cluster_cnt[cls_idx];
  }
}


__global__ void update_centers(ClusterCenter *global_center) {
  // note: this kernel uses 1 block and 1D thread (64 threads)
  int idx = threadIdx.x;
  double cnt = global_center[idx].cnt;
  global_center[idx].x = global_center[idx].sum_x / cnt;
  global_center[idx].y = global_center[idx].sum_y / cnt;
  global_center[idx].clr = global_center[idx].sum_clr / cnt;

  global_center[idx].sum_x = 0.0;
  global_center[idx].sum_y = 0.0;
  global_center[idx].sum_clr = 0.0;
  global_center[idx].cnt = 0.0;
}


int main( int argc, char *argv[] )
{

/* get GPU device number and name */

  int dev;
  cudaDeviceProp deviceProp;
  checkCUDA( cudaGetDevice( &dev ) );
  checkCUDA( cudaGetDeviceProperties( &deviceProp, dev ) );
  printf("Using GPU %d: %s\n", dev, deviceProp.name );

  const int size = SIZE;

  int *d_img;
  int *d_result;
  int *h_result;
  ClusterCenter *global_center; // on device


  size_t numbytes = size * size * sizeof( int );

  h_result = (int *) malloc( numbytes );
  if( h_result == NULL )
  {
    fprintf(stderr,"Error in host malloc\n");
    return 911;
  }

  checkCUDA( cudaMalloc( (void **)&d_img, numbytes ) );
  checkCUDA( cudaMalloc( (void **)&d_result, numbytes ) ); 
  checkCUDA( cudaMalloc( (void **)&global_center, SIZE_CLS * SIZE_CLS * sizeof(ClusterCenter) ) );

  checkCUDA( cudaMemcpy( d_img, img, numbytes, cudaMemcpyHostToDevice ) ); // img defined in the header file
  checkCUDA( cudaMemset( d_result, 0, numbytes ) );
  checkCUDA( cudaMemset( global_center, 0, SIZE_CLS * SIZE_CLS * sizeof(ClusterCenter) ) );

  // initialize centers, 1 thread / center, use 2D threads, 64 threads in total
  dim3 init_threads( SIZE_CLS, SIZE_CLS, 1 );
  init_centers<<< 1, init_threads >>> (global_center, d_img); 

  dim3 threads( 32, 32, 1 );
  dim3 blocks( 32, 32, 1 );

  // do 3 iterations
  for(int i = 0; i < 9; i++) {
    cluster_assign<<< blocks, threads >>> (global_center, d_img, d_result);
    update_centers<<< 1, 64 >>> (global_center); 
  }
  cluster_assign<<< blocks, threads >>> (global_center, d_img, d_result);

  // copy result back
  checkCUDA( cudaMemcpy( h_result, d_result, numbytes, cudaMemcpyDeviceToHost ) );

  // write to txt file
  ofstream myfile;
  myfile.open ("result.m");
  myfile << "im = [... \n";
  for(int i=0; i<size; i++) {
    for(int j=0; j<size; j++) {
      if(j != size-1)
        myfile << h_result[i*size + j] << ", ";
      else
        myfile << h_result[i*size + j];
    }
    myfile << ";\n";
  }
  myfile << "];" ;
  myfile.close();

  // free memory
  checkCUDA( cudaFree( d_img ) );
  checkCUDA( cudaFree( d_result ) );
  checkCUDA( cudaFree( global_center ) );

  free(h_result);

  return 0;
}
